import React from 'react';
import './App.css';
import { BrowserRouter as Router, Routes, Route, Link, NavLink, useParams, useLocation, useMatch } from "react-router-dom";

export default function App(){
  return (
    <Router>
      <div>
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/topics">Topics</Link>
          </li>
        </ul>
        <hr />
        <Routes>
          <Route exact path="/" element={<Home />}>            
          </Route>
          <Route path="/topics/*" element={<Topics />}>    
          </Route>
        </Routes>
      </div>
    </Router>
  );  
}

function Home() {
  return (
    <div>
      <h2>Home</h2>
    </div>
  );
}

function Topics(){
  return (
    <div>
      <h2>Topics</h2>
      <ul>
        <li>
          <Link to="rendering">Rendering with React</Link>
        </li>
        <li>
          <Link to="components">Components</Link>
        </li>
        <li>
          <Link to="props-v-state">Props v. State</Link>
        </li>
      </ul>
      <Routes>
        <Route exact path="/" element={<h3>Please select a topic.</h3>}></Route>
        <Route path=":topicId" element={<Topic />}></Route>
      </Routes>
    </div>
  );
}

function Topic() {
  let {topicId} = useParams();
  console.log(topicId);
  return (
    <div>
      <h3>{topicId}</h3>
    </div>
  );
}
